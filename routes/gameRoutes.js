import { Router } from 'express';
import path from 'path';
import { HTML_FILES_PATH } from '../config';
import { texts } from '../data.js';

const router = Router();

router
  .get('/', (req, res) => {
    const page = path.join(HTML_FILES_PATH, 'game.html');
    res.sendFile(page);
  })
  .get('/texts/:id', (req, res) => {
    res.data = texts[+req.params.id];
    if (res.data) res.status(200).json(res.data);
    else res.status(404).json({ error: true, message: 'No text with such index' });
  });

export default router;
