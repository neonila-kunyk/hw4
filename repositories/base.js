import mongoose from 'mongoose';

// eslint-disable-next-line import/prefer-default-export
export const base = (entity) => ({
  async getOne(parameters) {
    try {
      return await this.model.findOne(parameters);
    } catch (err) {
      console.error(`Can not get given ${entity}: ${err.message}`);
    }
  },

  async getSome(parameters) {
    try {
      return await this.model.find(parameters);
    } catch (err) {
      console.error(`Can not get given ${entity}s: ${err.message}`);
    }
  },

  async getAll() {
    try {
      return await this.model.find({});
    } catch (err) {
      console.error(`Can not get any ${entity}: ${err.message}`);
    }
  },

  async create(parameters) {
    try {
      // eslint-disable-next-line no-param-reassign
      parameters._id = new mongoose.Types.ObjectId();
      await this.model.create(parameters);
    } catch (err) {
      console.error(`Can not create ${entity} document: ${err.message}`);
    }
  },

  async updateOne(parameters, info) {
    try {
      await this.model.updateOne(parameters, info);
    } catch (err) {
      console.error(`Can not update any ${entity} document: ${err.message}`);
    }
  },

  async deleteOne(parameters) {
    try {
      await this.model.deleteOne(parameters);
    } catch (err) {
      console.error(`Can not delete any ${entity} document: ${err.message}`);
    }
  }
});
