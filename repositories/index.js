import repositoryFactory from './repository-factory.js';

export const Users = repositoryFactory.create('user');
export const Rooms = repositoryFactory.create('room');
