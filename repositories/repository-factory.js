import conn from '../db/key-racing.js';
import { base } from './base.js';

// FACTORY PATTERN //

function Rooms() {
  this.model = conn.models.rooms;
}

function Users() {
  this.model = conn.models.users;
}

export default {
  create: (entity) => {
    switch (entity.toLowerCase()) {
      case 'room':
        return Object.assign(new Rooms(), base(entity));
      case 'user':
        return Object.assign(new Users(), base(entity));
      default:
        return null;
    }
  }
};
