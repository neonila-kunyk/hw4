export default (socket) => {
  const relogin = () => {
    if (+sessionStorage.getItem('number_of_load') > 1) return;
    sessionStorage.clear();
    alert('ERROR: this username is already in use.');
    window.location.replace('/login');
  };

  socket.on('USERNAME_ALREADY_IN_USE', relogin);
  socket.on('ROOMNAME_ALREADY_IN_USE', () => alert('ERROR: room with this name is already exist.'));
  socket.on('ROOMNAME_IS_EMPTY', () => alert("ERROR: you can't create room with empty name."));
  socket.on('TOO_MANY_USERS', () => alert("ERROR: you can't join room with max number of users."));
};
