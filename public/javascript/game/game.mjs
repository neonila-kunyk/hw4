import { createUserCard, createPlayerItem } from './tools/create-DOM.mjs';
import { addClass, removeClass, sortByName } from './tools/helper.mjs';
import sendRequest from './tools/sender-req.mjs';

export default (socket) => {
  document.querySelector('#quit-results-btn').addEventListener('click', () => {
    addClass(document.querySelector('.popup'), 'display-none');
    removeClass(document.querySelector('#ready-btn'), 'display-none');
    removeClass(document.querySelector('#quit-room-btn'), 'visible-none');
  });

  const updateUsers = (users) => {
    const usersContainer = document.querySelector('.users-container');
    usersContainer.innerHTML = '';
    users.sort(sortByName);
    for (const user of users) {
      if (user.socketId === socket.id) {
        const userCard = createUserCard(user, true);
        usersContainer.append(userCard);
      } else {
        const userCard = createUserCard(user, false);
        usersContainer.append(userCard);
      }
    }
  };

  const changeStateDone = ({ isReady, id }) => {
    const button = document.querySelector('#ready-btn');
    const statusCircle = document.querySelector(`#status-of-${id}`);
    if (isReady) {
      if (socket.id === id) {
        button.innerHTML = 'NOT READY';
        button.style.background = 'radial-gradient(#fd8484, #f85656)';
      }
      addClass(statusCircle, 'ready-status-green');
      removeClass(statusCircle, 'ready-status-red');
    } else {
      if (socket.id === id) {
        button.innerHTML = 'READY';
        button.style.background = 'radial-gradient(#60efbc, #58d5c9)';
      }
      addClass(statusCircle, 'ready-status-red');
      removeClass(statusCircle, 'ready-status-green');
    }
  };

  const timerStart = () => {
    const timerElem = document.querySelector('#timer');
    addClass(document.querySelector('#ready-btn'), 'display-none');
    addClass(document.querySelector('#quit-room-btn'), 'visible-none');
    removeClass(timerElem, 'display-none');
    socket.emit('TIMER_STARTED');
  };

  const timerDecrease = (counter) => {
    const timerElem = document.querySelector('#timer');
    timerElem.innerHTML = counter;
  };

  const inputHandler = (e) => {
    socket.emit('INPUT_LETTER', e.key);
  };

  const receiveGameInfo = async (textIndex) => {
    // sendRequest(`https://key-racing.herokuapp.com/game/texts/${textIndex}`)
    const data = await sendRequest(`http://localhost:3002/game/texts/${textIndex}`);
    const timerElem = document.querySelector('#timer');
    const timeToEnd = document.querySelector('#time-to-end');
    const textContainer = document.querySelector('#text-container');
    const simpleTextContainer = document.querySelector('#text-container .simple');
    const underlinedTextContainer = document.querySelector('#text-container .underlined');
    timeToEnd.innerHTML = '';
    addClass(timerElem, 'display-none');
    removeClass(textContainer, 'display-none');
    removeClass(timeToEnd, 'display-none');
    simpleTextContainer.innerHTML = data.slice(1);
    [underlinedTextContainer.innerHTML] = [...data];
    document.addEventListener('keydown', inputHandler);
    socket.emit('RECEIVE_TEXT', data);
  };

  const raceTimerDecrease = ({ counter, time }) => {
    const timeToEnd = document.querySelector('#time-to-end');
    if (counter === 1) timeToEnd.innerHTML = `${counter} second left`;
    else if (counter >= 0) timeToEnd.innerHTML = `${counter} seconds left`;
    if (time % 30 === 0) socket.emit('30_SEC');
  };

  const rightLetter = ({ penult, last }) => {
    const simpleTextContainer = document.querySelector('#text-container .simple');
    const colorizedTextContainer = document.querySelector('#text-container .colorized');
    const underlinedTextContainer = document.querySelector('#text-container .underlined');
    colorizedTextContainer.innerHTML += underlinedTextContainer.innerHTML;
    if (!last) underlinedTextContainer.innerHTML = simpleTextContainer.innerHTML[0].replace(' ', '&nbsp');
    else underlinedTextContainer.innerHTML = '';
    if (!penult && !last) simpleTextContainer.innerHTML = simpleTextContainer.innerHTML.slice(1);
    else simpleTextContainer.innerHTML = '';
    if (simpleTextContainer.innerHTML.length === 30) socket.emit('30_SYM_TO_END');
    if (simpleTextContainer.innerHTML.length === 10) socket.emit('AT_FINISH');
  };

  const finishRace = (id) => {
    const userProgress = document.querySelector(`.user-progress.id-${id}`);
    addClass(userProgress, 'finish');
  };

  const endGame = (rating) => {
    const textContainer = document.querySelector('#text-container');
    const timeToEnd = document.querySelector('#time-to-end');
    addClass(textContainer, 'display-none');
    addClass(timeToEnd, 'display-none');
    const colorizedTextContainer = document.querySelector('#text-container .colorized');
    colorizedTextContainer.innerHTML = '';
    document.removeEventListener('keydown', inputHandler);
    removeClass(document.querySelector('.popup'), 'display-none');
    const items = rating.map(createPlayerItem);
    const list = document.querySelector('.popup-list');
    list.innerHTML = '';
    list.append(...items);
    socket.emit('GAME_ENDED');
  };

  const newComment = (text) => {
    const comment = document.querySelector('.comment');
    const joke = document.querySelector('.joke');
    comment.innerHTML = text;
    addClass(joke, 'display-none');
    removeClass(comment, 'display-none');
    speechSynthesis.cancel();
    const utterance = new SpeechSynthesisUtterance(text.replace(/<br \/>/g, ''));
    setTimeout(() => {
      utterance.voice = speechSynthesis.getVoices().find((voice) => voice.voiceURI === 'Google UK English Male');
      console.info(window.speechSynthesis.getVoices());
      speechSynthesis.speak(utterance);
    }, 10);
  };

  const gameFinished = async () => {
    const data = await sendRequest('https://official-joke-api.appspot.com/jokes/programming/random');
    const [jokeObj] = [...data];
    const comment = document.querySelector('.comment');
    comment.innerHTML = '';
    const joke = document.querySelector('.joke');
    joke.innerHTML = `${jokeObj.setup} ${jokeObj.punchline} ${joke.innerHTML}`;
    addClass(comment, 'display-none');
    removeClass(joke, 'display-none');
    socket.emit('END_GAME_DONE');
  };

  socket.on('UPDATE_USERS', updateUsers);
  socket.on('CHANGE_STATE_DONE', changeStateDone);
  socket.on('TIMER_START', timerStart);
  socket.on('TIMER_DECREASE', timerDecrease);
  socket.on('RECEIVE_GAME_INFO', receiveGameInfo);
  socket.on('RACE_TIMER_DECREASE', raceTimerDecrease);
  socket.on('RIGHT_LETTER', rightLetter);
  socket.on('FINISH_RACE', finishRace);
  socket.on('END_GAME', endGame);
  socket.on('NEW_COMMENT', newComment);
  socket.on('GAME_FINISHED', gameFinished);
};
