const formatClassNames = (className) => className.split(' ').filter(Boolean);

export const addClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.add(...classNames);
};

export const removeClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.remove(...classNames);
};

export const createElement = ({ tagName, className, attributes = {} }) => {
  const element = document.createElement(tagName);
  if (className) addClass(element, className);
  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));
  return element;
};

export const getRandom = (min, max) => {
  const minCeil = Math.ceil(min);
  const maxFloor = Math.floor(max);
  return Math.floor(Math.random() * (maxFloor - minCeil + 1)) + minCeil;
};

export const sortByName = (a, b) => {
  if (a.name > b.name) return 1;
  if (a.name < b.name) return -1;
  return 0;
};
