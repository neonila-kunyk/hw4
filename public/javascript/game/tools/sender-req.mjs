export default async (url) => {
  const headers = {
    'Content-Type': 'application/json'
  };
  const response = await fetch(url, { headers });
  if (!response.ok) console.error(response.statusText);
  else return response.json();
};
