import sendRequest from './sender-req.mjs';

// PROXY PATTERN //

export default new Proxy(sendRequest, {
  apply(target, thisArg, args) {
    const url = args[0];
    if (url === 'https://official-joke-api.appspot.com/jokes/programming/random') {
      const data = Reflect.apply(target, thisArg, args);
      const [jokeObj] = [...data];
      const joke = document.querySelector('.joke');
      if (joke.innerHTML === `${jokeObj.setup} ${jokeObj.punchline} ${joke.innerHTML}`) {
        return Reflect.apply(target, thisArg, args);
      }
      return data;
    }
    return Reflect.apply(target, thisArg, args);
  }
});
