import { createRoomCard } from './tools/create-DOM.mjs';
import { addClass, removeClass } from './tools/helper.mjs';

export default (socket) => {
  document.querySelector('#add-room-btn').addEventListener('click', () => {
    const roomName = prompt('Enter room name, please:');
    socket.emit('ADD_ROOM', roomName);
  });

  const updateRooms = (rooms) => {
    const roomsContainer = document.querySelector('.rooms-container');
    roomsContainer.innerHTML = '';
    for (const room of rooms) {
      const roomCard = createRoomCard({ name: room.name, numberOfUsers: room.users.length, socket });
      roomsContainer.append(roomCard);
    }
  };

  const addRoomDone = (roomName, currentUserId) => {
    const roomsContainer = document.querySelector('.rooms-container');
    const roomCard = createRoomCard(roomName, 1, socket);
    roomsContainer.append(roomCard);
    if (currentUserId === socket.id) socket.emit('JOIN_ROOM', roomName);
  };

  const joinRoomDone = (roomName) => {
    addClass(document.querySelector('#rooms-page'), 'display-none');
    removeClass(document.querySelector('#game-page'), 'display-none');
    document.querySelector('#game-page .page-title').innerHTML = roomName;
    document.querySelector('#quit-room-btn').addEventListener('click', () => {
      socket.emit('QUIT_ROOM', roomName);
    });
    document.querySelector('#ready-btn').addEventListener('click', () => {
      socket.emit('CHANGE_STATE');
    });
    socket.emit('ROOM_JOINED');
  };

  const quitRoomDone = () => {
    addClass(document.querySelector('#game-page'), 'display-none');
    removeClass(document.querySelector('#rooms-page'), 'display-none');
  };

  socket.on('UPDATE_ROOMS', updateRooms);
  socket.on('ADD_ROOM_DONE', addRoomDone);
  socket.on('JOIN_ROOM_DONE', joinRoomDone);
  socket.on('QUIT_ROOM_DONE', quitRoomDone);
};
