import * as config from './config.js';
import setTimer from './tools/timer.js';
import {
  checkIsAllReady,
  checkIsAllFinished,
  fillRatingAfterTimeout,
  formatRating,
  sortByTimeAndPosition
} from './tools/helper.js';
import filterVisibleRooms from './tools/filter.js';
import { Users, Rooms } from '../repositories/index.js';

export default async (io, socket) => {
  const changeState = async () => {
    const user = await Users.getOne({ socketId: socket.id });
    user.isReady = !user.isReady;
    await Users.updateOne({ socketId: socket.id }, { isReady: user.isReady });
    const users = await Users.getSome({ room: user.room });
    if (checkIsAllReady(users)) {
      io.emit('UPDATE_ROOMS', await filterVisibleRooms(await Rooms.getAll()));
      io.to(user.room.toString()).emit('TIMER_START');
      setTimer(io, socket);
    } else {
      io.to(user.room.toString()).emit('CHANGE_STATE_DONE', { isReady: user.isReady, id: user.socketId });
    }
  };

  const timeIsOut = async () => {
    const user = await Users.getOne({ socketId: socket.id });
    if (user) {
      const room = await Rooms.getOne({ _id: user.room });
      if (room.timerId) {
        clearInterval(room.timerId);
        room.timerId = null;
        await Rooms.updateOne({ _id: user.room }, { timerId: room.timerId });
        const users = await Users.getSome({ room: room._id });
        room.rating = fillRatingAfterTimeout(room.rating, users);
        await Rooms.updateOne({ _id: user.room }, { rating: room.rating });
        const ratingFiltered = room.rating.sort(sortByTimeAndPosition).map(formatRating).filter((el) => !!el);
        io.to(room._id.toString()).emit('END_GAME', ratingFiltered);
      }
    }
  };

  const receiveText = async (text) => {
    const user = await Users.getOne({ socketId: socket.id });
    const room = await Rooms.getOne({ _id: user.room });
    await Rooms.updateOne({ name: room.name }, { text });
    let counter = config.SECONDS_FOR_GAME;
    io.to(room._id.toString()).emit('RACE_TIMER_DECREASE', counter);
    if (!room.timerId) {
      room.timerId = setInterval(async () => {
        room.time = config.SECONDS_FOR_GAME - counter;
        await Rooms.updateOne({ _id: user.room }, { time: room.time });
        if (counter >= 0) io.to(room._id.toString()).emit('RACE_TIMER_DECREASE', { counter: --counter, time: room.time });
        else {
          timeIsOut();
        }
      }, 1000);
      await Rooms.updateOne({ _id: user.room }, { timerId: room.timerId });
    }
  };

  const inputLetter = async (key) => {
    const user = await Users.getOne({ socketId: socket.id });
    const room = await Rooms.getOne({ _id: user.room });
    if (room.text[user.position] === key) {
      user.progress = (++user.position * 100) / room.text.length;
      await Users.updateOne({ socketId: socket.id }, { progress: user.progress, position: user.position });
      let penult = false;
      let last = false;
      const users = await Users.getSome({ room: room._id });
      io.to(room._id.toString()).emit('UPDATE_USERS', users);
      if (user.position === room.text.length) last = true;
      else if (user.position === room.text.length - 1) penult = true;
      socket.emit('RIGHT_LETTER', { penult, last });
      if (last) {
        io.to(room._id.toString()).emit('FINISH_RACE', socket.id);
        room.rating.push({ time: room.time, name: user.name, position: user.position });
        await Rooms.updateOne({ name: room.name }, { rating: room.rating });
        if (checkIsAllFinished(users)) {
          timeIsOut();
        }
      }
    }
  };

  const endGameDone = async () => {
    const user = await Users.getOne({ socketId: socket.id });
    // await Rooms.updateOne({ _id: user.room }, { rating: [] });
    const users = await Users.getSome({ room: user.room });
    for (const _user of users) {
      await Users.updateOne({ _id: _user._id }, { progress: 0, position: 0, isReady: false });
    }
    const usersUpdated = await Users.getSome({ room: user.room });
    io.to(user.room.toString()).emit('UPDATE_USERS', usersUpdated);
    io.emit('UPDATE_ROOMS', await filterVisibleRooms(await Rooms.getAll()));
    for (const userUpdated of usersUpdated) {
      io.to(user.room.toString()).emit('CHANGE_STATE_DONE', { isReady: userUpdated.isReady, id: userUpdated.socketId });
    }
  };

  socket.on('INPUT_LETTER', inputLetter);
  socket.on('CHANGE_STATE', changeState);
  socket.on('RECEIVE_TEXT', receiveText);
  socket.on('END_GAME_DONE', endGameDone);
};
