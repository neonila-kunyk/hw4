/* eslint-disable no-unused-vars */
import get from './tools/comment-facade';
import { Users } from '../repositories/index.js';

export default async (io, socket) => {
  const roomJoined = async () => {
    const user = await Users.getOne({ socketId: socket.id });
    const text = await get('greeting', user);
    io.to(user.room.toString()).emit('NEW_COMMENT', text);
  };

  const timerStarted = async () => {
    const user = await Users.getOne({ socketId: socket.id });
    const text = await get('players-list', user);
    io.to(user.room.toString()).emit('NEW_COMMENT', text);
  };

  const _30Sec = async () => {
    const user = await Users.getOne({ socketId: socket.id });
    const text = await get('30sec', user);
    io.to(user.room.toString()).emit('NEW_COMMENT', text);
  };

  const _30SymToEnd = async () => {
    const user = await Users.getOne({ socketId: socket.id });
    const text = await get('30sym', user);
    io.to(user.room.toString()).emit('NEW_COMMENT', text);
  };

  const finish = async () => {
    const user = await Users.getOne({ socketId: socket.id });
    const text = await get('finish', user);
    io.to(user.room.toString()).emit('NEW_COMMENT', text);
  };

  const gameEnded = async () => {
    const user = await Users.getOne({ socketId: socket.id });
    const text = await get('end', user);
    io.to(user.room.toString()).emit('NEW_COMMENT', text);
    socket.emit('GAME_FINISHED');
  };

  socket.on('ROOM_JOINED', roomJoined);
  socket.on('TIMER_STARTED', timerStarted);
  socket.on('30_SEC', _30Sec);
  socket.on('30_SYM_TO_END', _30SymToEnd);
  socket.on('AT_FINISH', finish);
  socket.on('GAME_ENDED', gameEnded);
};
