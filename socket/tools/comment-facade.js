import commentListFactory from './comment-list-factory';
import commentPlayerFactory from './comment-player-factory';
import commentRatingFactoryCopy from './comment-rating-factory copy';
import { formatRating, formatUsers, sortByTimeAndPosition } from './helper.js';
import { Users, Rooms } from '../../repositories/index.js';

// FACADE PATTERN //

const greeting = async (player) => {
  const users = await Users.getSome({ room: player.room });
  return commentListFactory.get('greeting', users).text;
};

const playersList = async (player) => {
  const users = await Users.getSome({ room: player.room });
  return commentListFactory.get('players-list', users).text;
};

const _30sec = async (player) => {
  const users = await Users.getSome({ room: player.room });
  return commentListFactory.get('30sec', users.map(formatUsers)).text;
};

const _30sym = async (player) => commentPlayerFactory.get('30sym', player).text;

const finish = async (player) => commentPlayerFactory.get('finish', player).text;

const end = async (player) => {
  const room = await Rooms.getOne({ _id: player.room });
  const ratingFiltered = room.rating.sort(sortByTimeAndPosition).map(formatRating).filter((el) => !!el);
  return commentRatingFactoryCopy.get('end', ratingFiltered).text;
};

export default async (type, player) => {
  switch (type.toLowerCase()) {
    case 'greeting':
      return await greeting(player);
    case 'players-list':
      return await playersList(player);
    case '30sec':
      return await _30sec(player);
    case '30sym':
      return await _30sym(player);
    case 'finish':
      return await finish(player);
    case 'end':
      return await end(player);
    default:
      return null;
  }
};
