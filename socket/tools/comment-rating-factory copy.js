/* eslint-disable max-len */
// FACTORY PATTERN //

function EndGame(rating) {
  let str = '';
  if (rating) {
    str = 'The final result is:<br />';
    for (const position of rating) {
      str += `${rating.indexOf(position) + 1}. ${position.join(', ')}<br />`;
    }
  }
  this.text = str;
}

export default {
  get: (type, rating) => {
    if (type.toLowerCase() === 'end') return new EndGame(rating);
    return null;
  }
};
