import { Users } from '../../repositories/index.js';
import { checkIsAllReady } from './helper.js';
import * as config from '../config.js';

export default async (rooms) => rooms
  .filter(async (room) => !(room.users.length === config.MAXIMUM_USERS_FOR_ONE_ROOM
  || checkIsAllReady(await Users.getSome({ room: room._id }))));
