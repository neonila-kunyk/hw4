import { texts } from '../../data.js';
import { Users, Rooms } from '../../repositories/index.js';
import { getRandom } from './helper.js';
import * as config from '../config.js';

export default async (io, socket) => {
  const user = await Users.getOne({ socketId: socket.id });
  if (user?.room) {
    const room = await Rooms.getOne({ _id: user.room });
    let counter = config.SECONDS_TIMER_BEFORE_START_GAME;
    io.to(room._id.toString()).emit('TIMER_DECREASE', counter--);
    let timer = setInterval(() => {
      if (counter >= 0) io.to(room._id.toString()).emit('TIMER_DECREASE', counter--);
      else {
        clearInterval(timer);
        timer = null;
        const textIndex = getRandom(0)(texts.length - 1);
        io.to(room._id.toString()).emit('RECEIVE_GAME_INFO', textIndex);
      }
    }, 1000);
  }
};
