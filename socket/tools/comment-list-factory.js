/* eslint-disable max-len */
// FACTORY PATTERN //

function Greeting(users) {
  if (users) this.text = `Good day, dear racing fans! Your attention is invited to an incredible arrival with ${users.length} players. The motors growl, the participants are determined only to win. An exciting spectacle awaits us, I will comment on it - Escape Enter.`;
}

function PlayersList(users) {
  let str = '';
  if (users) {
    str = 'Well, let\'s list today\'s participants:<br />';
    for (const user of users) {
      str += `- on a ferrari with number ${users.indexOf(user) + 1} is ${user.name}.<br />`;
    }
  }
  this.text = str;
}

function ThirtySecond(users) {
  let str = '';
  if (users) {
    str = 'Now the situation is like this:<br />';
    for (const username of users) {
      str += `${users.indexOf(username) + 1}. is ${username.join(', ')}<br />`;
    }
  }
  this.text = str;
}

export default {
  get: (type, users) => {
    switch (type.toLowerCase()) {
      case 'greeting':
        return new Greeting(users);
      case 'players-list':
        return new PlayersList(users);
      case '30sec':
        return new ThirtySecond(users);
      default:
        return null;
    }
  }
};
