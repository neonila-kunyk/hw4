import {
  checkIsAllReady,
  checkIsAllFinished,
  fillRatingAfterTimeout,
  formatRating,
  sortByTimeAndPosition
} from './helper.js';
import filterVisibleRooms from './filter.js';
import setTimer from './timer.js';
import { Users, Rooms } from '../../repositories/index.js';

export default async (socket, io) => {
  const user = await Users.getOne({ socketId: socket.id });
  const room = await Rooms.getOne({ _id: user.room });
  if (room.users.length <= 1 && room.timerId) {
    clearInterval(room.timerId);
    room.timerId = null;
    await Rooms.updateOne({ _id: user.room }, { timerId: room.timerId });
  }

  if (room.rating.length !== 0) room.rating.splice(room.rating.indexOf(user), 1);
  room.users.splice(room.users.indexOf(user._id), 1);
  await Rooms.updateOne({ name: room.name }, { users: room.users, rating: room.rating });
  await Users.updateOne({ _id: user._id }, { room: null });
  io.emit('UPDATE_ROOMS', await filterVisibleRooms(await Rooms.getAll()));
  const users = await Users.getSome({ room: room._id });
  io.to(room._id.toString()).emit('UPDATE_USERS', users);

  if (room.users.length === 0) {
    await Rooms.deleteOne({ _id: room._id });
    io.emit('UPDATE_ROOMS', await filterVisibleRooms(await Rooms.getAll()));
  } else if (checkIsAllFinished(users)) {
    room.rating = fillRatingAfterTimeout(room.rating, users);
    await Rooms.updateOne({ _id: user.room }, { rating: room.rating });
    const ratingFiltered = rating.sort(sortByTimeAndPosition).map(formatRating).filter((el) => !!el);
    io.to(room._id.toString()).emit('END_GAME', ratingFiltered);
  } else if (checkIsAllReady(users)) {
    io.to(room._id.toString()).emit('TIMER_START');
    setTimer(io, socket);
  }
};
