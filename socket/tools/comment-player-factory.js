/* eslint-disable max-len */
// FACTORY PATTERN //

function ThirtySymbols(player) {
  this.text = `30 characters left to the finish line for the player: ${player.name}`;
}

function AtFinish(player) {
  this.text = `At the finish line is player: ${player.name}`;
}

export default {
  get: (type, player) => {
    switch (type.toLowerCase()) {
      case '30sym':
        return new ThirtySymbols(player);
      case 'finish':
        return new AtFinish(player);
      default:
        return null;
    }
  }
};
