import partial from 'lodash.partial';
import curry from 'lodash.curry';
import * as config from '../config.js';

export const checkIsAllReady = (users) => {
  let isAllReady = true;
  for (const user of users) {
    if (user.isReady === false) isAllReady = false;
  }
  return isAllReady;
};

export const checkIsAllFinished = (users) => {
  let isAllReady = true;
  for (const user of users) {
    if (user.progress !== 100) isAllReady = false;
  }
  return isAllReady;
};

export const getRandom = curry((min, max) => {
  const minCeil = Math.ceil(min);
  const maxFloor = Math.floor(max);
  return Math.floor(Math.random() * (maxFloor - minCeil + 1)) + minCeil;
});

export const fillRatingAfterTimeout = (rating, users) => {
  const filled = [...rating];
  for (const user of users) {
    let player;
    for (const note of rating) {
      if (note?.name === user.name) player = user;
    }
    if (!player) filled.push({ time: config.SECONDS_FOR_GAME, name: user.name, position: user.position });
  }
  return filled;
};

export const sortByTimeAndPosition = (a, b) => {
  if (a.time === b.time) {
    if (a.position > b.position) return -1;
    if (a.position < b.position) return 1;
  } else {
    if (a.time > b.time) return 1;
    if (a.time < b.time) return -1;
  }
  return 0;
};

export const sortByPosition = (a, b) => {
  if (a.position > b.position) return -1;
  if (a.position < b.position) return 1;
  return 0;
};

const formatData = (areUsers, el, index, array) => {
  if (array) {
    const username = [];
    username.push(el.name);
    for (let i = index + 1; i < array.length; i++) {
      if ((areUsers && el.position === array[i].position)
        || (!areUsers && el.time === array[i].time && el.position === array[i].position)) {
        username.push(array[i].name);
        array.splice(i, 1);
      }
    }
    return username;
  }
};

export const formatRating = partial(formatData, false);

export const formatUsers = partial(formatData, true);
