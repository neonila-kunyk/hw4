import * as config from './config.js';
import { checkIsAllReady } from './tools/helper.js';
import filterVisibleRooms from './tools/filter.js';
import setTimer from './tools/timer.js';
import { Users, Rooms } from '../repositories/index.js';

export default async (io, socket) => {
  const addRoom = async (roomName) => {
    const user = await Users.getOne({ socketId: socket.id });
    const room = await Rooms.getOne({ name: roomName });
    if (room) socket.emit('ROOMNAME_ALREADY_IN_USE');
    else if (!roomName) socket.emit('ROOMNAME_IS_EMPTY');
    else {
      await Rooms.create({ name: roomName });
      io.emit('ADD_ROOM_DONE', roomName, user.socketId);
    }
  };

  const joinRoom = async (roomName) => {
    const user = await Users.getOne({ socketId: socket.id });
    const room = await Rooms.getOne({ name: roomName });
    if (room.users.length === config.MAXIMUM_USERS_FOR_ONE_ROOM) socket.emit('TOO_MANY_USERS', room.name);
    else {
      room.users.push(user._id);
      await Rooms.updateOne({ name: room.name }, { users: room.users });
      await Users.updateOne({ _id: user._id }, { room: room._id });
      socket.join(room._id.toString(), async () => {
        socket.broadcast.emit('UPDATE_ROOMS', await filterVisibleRooms(await Rooms.getAll()));
        socket.emit('JOIN_ROOM_DONE', room.name);
        io.to(room._id.toString()).emit('UPDATE_USERS', await Users.getSome({ room: room._id }));
      });
    }
  };

  const quitRoom = async (roomName) => {
    const user = await Users.getOne({ socketId: socket.id });
    const room = await Rooms.getOne({ name: roomName });
    await Rooms.updateOne({ name: room.name }, { users: room.users.splice(room.users.indexOf(user._id), 1) });
    await Users.updateOne({ _id: user._id }, { room: null });
    io.emit('UPDATE_ROOMS', await filterVisibleRooms(await Rooms.getAll()));
    socket.leave(room._id.toString(), async () => {
      if (room.users.length === 0) {
        await Rooms.deleteOne({ name: roomName });
        io.emit('UPDATE_ROOMS', await filterVisibleRooms(await Rooms.getAll()));
      } else {
        const users = await Users.getSome({ room: room._id });
        io.to(room._id.toString()).emit('UPDATE_USERS', users);
        socket.emit('QUIT_ROOM_DONE');
        if (checkIsAllReady(users)) {
          io.to(room._id.toString()).emit('TIMER_START', config.SECONDS_TIMER_BEFORE_START_GAME);
          setTimer(io, socket);
        }
      }
    });
  };

  socket.on('ADD_ROOM', addRoom);
  socket.on('JOIN_ROOM', joinRoom);
  socket.on('QUIT_ROOM', quitRoom);
};
