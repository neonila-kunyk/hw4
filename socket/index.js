import initRooms from './rooms.js';
import initComment from './comment.js';
import initGame from './game.js';
import exitRoom from './tools/exit.js';
import filterVisibleRooms from './tools/filter.js';
import { Users, Rooms } from '../repositories/index.js';

export default (io) => {
  io.on('connection', async (socket) => {
    socket.emit('UPDATE_ROOMS', await filterVisibleRooms(await Rooms.getAll()));

    {
      const { username } = socket.handshake.query;
      const user = await Users.getOne({ name: username });
      if (user) socket.emit('USERNAME_ALREADY_IN_USE');
      else {
        await Users.create({
          socketId: socket.id, name: username, position: 0, progress: 0, isReady: false, room: null
        });
      }
    }

    await initRooms(io, socket);
    await initGame(io, socket);
    await initComment(io, socket);

    socket.on('disconnect', async () => {
      const user = await Users.getOne({ socketId: socket.id });
      if (user.room) await exitRoom(socket, io);
      await Users.deleteOne({ socketId: socket.id });
    });
  });
};
