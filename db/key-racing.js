import dotenv from 'dotenv';
import connectionPool from './database.js';
import roomSchema from '../schemas/room.js';
import userSchema from '../schemas/user.js';

dotenv.config();
let conn = null;

try {
  conn = connectionPool(`mongodb+srv://nelia:${process.env.BD_PASSWORD}@${process.env.CLUSTER}`, 'key-racing');
  conn.model('rooms', roomSchema, 'rooms');
  conn.model('users', userSchema, 'users');
} catch (err) {
  console.error('Error occurred during an attempt to establish connection with the database');
  console.error(err);
}

const connection = conn;
export default connection;
