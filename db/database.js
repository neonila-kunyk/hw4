import mongoose from 'mongoose';

export default (host, db) => {
  const opts = {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    poolSize: 5
  };
  return mongoose.createConnection(`${host}/${db}`, opts);
};
