import { Schema } from 'mongoose';

const { ObjectId } = Schema.Types;

export default new Schema({
  _id: ObjectId,
  socketId: String,
  name: String,
  position: Number,
  progress: Number,
  isReady: Boolean,
  room: ObjectId
}, { versionKey: false });
