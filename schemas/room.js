import { Schema } from 'mongoose';

const { ObjectId } = Schema.Types;

export default new Schema({
  _id: ObjectId,
  name: String,
  text: String,
  users: [ObjectId],
  rating: [{
    time: Number,
    name: String,
    position: Number
  }],
  timerId: Number,
  time: Number
}, { versionKey: false });
